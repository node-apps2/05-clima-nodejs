
const axios = require('axios');

let getLtLog = async (NombreCiudad) => {

    const encodeNombre = encodeURI(NombreCiudad);

    const instance = axios.create({
        baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${encodeNombre}`,
        headers: { 'x-rapidapi-key': '3a025503b9mshf70558d2f316f96p17af24jsnf8fa17dcdc53' },
    });

    const resp = await instance.get();

    if (resp.data.Results.lenght === 0) {
        throw new Error('No hay concidencias')
    }

    const data = resp.data.Results[0];
    const direccion = data.name;
    const lat = data.lat
    const lng = data.lon;

    return {
        direccion,
        lat,
        lng
    }

}


module.exports = { getLtLog }
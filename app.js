const argv = require("yargs").options({
  direccion: {
    demand: true,
    alias: "d",
    descripcion: "direccion de la ciudad para obtener el clima"
  }
}).argv;

const lugar = require('./Lugar/Lugar')
const clima = require('./clima/clima')

// lugar.getLtLog(argv.direccion).then(resp => {
//   console.log(resp);
// })

// clima.getClima(40.750000, -74.000000).then((resp) => {
//   console.log(resp);
// })

const getTemp = async (dir) => {
  try {
    const latLon = await lugar.getLtLog(dir);
    const temp = await clima.getClima(latLon.lat, latLon.lng);

    console.log(`La temperatura de ${dir} es ${temp.temp}`);

  } catch (error) {
    console.log(`No se han encontrados coincidencias con  ${dir}`);
  }

}

getTemp(argv.direccion);
const axios = require('axios');

const api_key = '9e3c2e9c9abaf44377ea0068833abdc5'
const units = 'metric'
let getClima = async (lat, long) => {
    const resp = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&lang=es&units=${units}&appid=${api_key}`)
    return resp.data.main;

}
module.exports = { getClima }
